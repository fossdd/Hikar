/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.GeomagneticField
import android.hardware.SensorManager
import android.location.Location
import android.opengl.Matrix
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup.LayoutParams
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import freemap.data.GoogleProjection
import freemap.data.POI
import freemap.data.Point
import freemap.datasource.FreemapDataset
import freemap.hikar.camera.CameraInterface
import freemap.hikar.opengl.OpenGLView

import freemap.hikar.datasource.OsmDemIntegrator
import freemap.hikar.location.LocationProcessor
import freemap.hikar.sensor.SensorInput
import freemap.hikar.signposting.Sign
import freemap.hikar.signposting.SignpostManager
import freemap.hikar.ui.*
import io.fotoapparat.Fotoapparat
import io.fotoapparat.selector.back
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

fun Fotoapparat.startWithTexCheck(gl: OpenGLView) {
    if (gl.renderer.isTextureAvailable()) {
        start()
    }
}

class MainActivity : CoroutineActivity() {

    private lateinit var signpostMgr: SignpostManager
    private val projection = GoogleProjection()
    private var integrator = OsmDemIntegrator(
        "https://hikar.org/fm/ws/tsvr.php?x={x}&y={y}&z={z}&outProj=3857&way=highway&ann=1&poi=place,amenity,natural,railway,tourism",
        "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png"
    )
    private val sensorInput = SensorInput(this::receiveSensorInput)
    private lateinit var locationProcessor: LocationProcessor
    private var lastLon = -0.72
    private var lastLat = 51.05
    //  private var lastLon = -1.6244
    //   private var lastLat = 50.8839
    // private var lastLon = -1.40883
    //private var lastLat = 50.92892
   // private var lastLon = -1.4131
  //  private var lastLat = 50.9416
  //  private var lastLon = -1.5029
   // private var lastLat = 50.83
   // private var lastLon = -1.5290
    //private var lastLat = 50.8899

    private var field: GeomagneticField? = null
    private lateinit var hud: HUD
    private lateinit var status: Status

    private var enableOrientationAdjustment = true
    private var orientationAdjustment = 0.0f

    private var runningUpdate = false

    //val testPoint = Point(-0.7263, 51.0503)
    // val testPoint = Point(-1.4, 50.9)

    //val testPoint = Point(-3.0376, 54.4340)
    //val testPoint = Point(-0.7286, 51.05172)
    // val testPoint = Point(-0.7343, 51.0461)
    //val testPoint = Point(-1.3216, 51.0217)
    // val testPoint = Point(-1.4118, 50.9346)

    //val testPoint = Point(-1.5290, 50.8899)


    private val initHfov = 40.0f

    enum class PermissionState { NOT_GRANTED, GRANTED, DENIED }

    private var permissionState = PermissionState.NOT_GRANTED


    private var fotoapparat: Fotoapparat? = null
    private lateinit var cameraInterface: CameraInterface

    private var lastNetworkErrorTime = 0L

    private var routingMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hud = HUD(this)
        status = Status(statusView)

        openGLView.onCreate(
            projection
        ) {

            if (permissionState == PermissionState.GRANTED) {

                fotoapparat?.start()
            }

        }

        openGLView.renderer.setHFOV(initHfov)
        status.setHfov(initHfov)


        signpostMgr = SignpostManager()
        addContentView(hud, LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT))

        locationProcessor = LocationProcessor(this, 1000, 1.0f, ::receiveLocation)

        sensorInput.attach(this)

        openGLView.setOnTouchListener(PinchListener {
            openGLView.renderer.changeHFOV((5 * it).toFloat())
            status.updateHfov((5 * it).toFloat())
            hud.invalidate()

        })

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        fotoapparat = Fotoapparat(context = this, view = openGLView.renderer, lensPosition = back())

        cameraInterface = CameraInterface(this) {
            openGLView.renderer.setHFOV(it)
            status.setHfov(it)
        }
    }

    override fun onResume() {
        super.onResume()


        val permissions = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        if (permissions.all {
                ContextCompat.checkSelfPermission(
                    this,
                    it
                ) == PackageManager.PERMISSION_GRANTED
            }) {

            permissionState = PermissionState.GRANTED

            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            var lang = prefs.getString("lang", "default") ?: ""
            lang =  if (lang == "default") "" else lang
            val distUnit = prefs.getString("distUnit", "km") ?: "km"
            if(signpostMgr.locale != lang || signpostMgr.distUnit != distUnit) {
                signpostMgr.locale = lang
                signpostMgr.distUnit = distUnit
                signpostMgr.clearSigns()
            }
            val zoomPref = (prefs.getString("zoomLevel", "13") ?: "13").toInt()
            if(zoomPref != integrator.zoom) {
                integrator.zoom = zoomPref
                openGLView.renderer.clearOsmData()
                signpostMgr.clearSigns()
            }

            fotoapparat?.startWithTexCheck(openGLView)
            cameraInterface.getHfov()
            //  gl.onResume()

            sensorInput.start()

            locationProcessor.startUpdates()

            openGLView.renderer.reRenderSigns(signpostMgr.allSigns)

        } else if (permissionState != PermissionState.DENIED) {

            ActivityCompat.requestPermissions(
                this, permissions, 0
            )
        }

    }

    override fun onPause() {
        super.onPause()
        locationProcessor.stopUpdates()
        sensorInput.stop()
        fotoapparat?.stop()
    }

    override fun onDestroy() {
        sensorInput.detach()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val routeItem = menu?.findItem(R.id.routeToPoi)
        routeItem?.title =
            if (routingMode) resources.getString(R.string.routeToPoiOff) else resources.getString(R.string.routeToPoi)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.preDownload -> {
                val intent = Intent(this, LocationPickerActivity::class.java)
                intent.putExtra("lat", lastLat)
                intent.putExtra("lon", lastLon)
                startActivityForResult(intent, 1)
            }

            R.id.routeToPoi -> {
                if (!routingMode) {
                    startActivityForResult(Intent(this, SearchPoiActivity::class.java), 0)
                } else {
                    openGLView.renderer.routeOff()
                    item.title = "Route to given POI"
                    routingMode = false
                    hud.stopRouting()
                }
            }

            R.id.addNoticeboard -> {
                with(Intent(this, AddNoticeboardActivity::class.java)) {
                    putExtra("lat", lastLat)
                    putExtra("lon", lastLon)
                    startActivity(this)
                }
            }

            R.id.userGuide -> {
                startActivity(Intent(this, UserGuide::class.java))
            }

            R.id.clearAllSavedData -> {
                integrator.deleteOsmData()
            }

            R.id.preferences -> {
                startActivity(Intent(this, Preferences::class.java))
            }

            R.id.about -> {
                AlertDialog.Builder(this)
                    .setMessage("Hikar 0.3.1, (c) Nick Whitelegg 2013-19. Uses OpenStreetMap data, copyright OpenStreetMap contributors, licensed under the Open Database License. Uses former-Mapzen Terrarium elevation data, freely available via Amazon Web Services.")
                    .setPositiveButton("OK", null).show()
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when (requestCode) {
            0 -> {
                if (resultCode == RESULT_OK) {
                    val osmId = intent?.getLongExtra("poi_id", 0) ?: 0
                    if (osmId > 0) {
                        var route: List<Point>? = null
                        var poi: POI? = null

                        launch {
                            withContext(Dispatchers.Default) {
                                poi = integrator.getCurrentOSMTiles().findPOIById(osmId)
                                if (poi != null) {
                                    val curPosition = Point(lastLon, lastLat)
                                    curPosition.z = integrator.getHeight(curPosition)
                                    route = signpostMgr.graphMgr.routeToPoi(curPosition, poi!!)
                                }
                            }

                            if (route.isNullOrEmpty()) {
                                AlertDialog.Builder(this@MainActivity)
                                    .setMessage("Could not find a route to ${poi!!.getValue("name")}")
                                    .setPositiveButton("OK", null).show()
                            } else {
                                withContext(Dispatchers.Default) {
                                    route!!.filter { it.z < 0 || it.z == Double.MAX_VALUE }.forEach {
                                        it.z = integrator.getHeight(it)
                                    }
                                }
                                openGLView.renderer.setRoute(route!!)
                                hud.setRoutingLocation(poi!!.getValue("name"))
                                routingMode = true
                                invalidateOptionsMenu()
                            }

                        }

                    }
                }
            }

            1 -> {
                if (locationProcessor.isProviderEnabled()) {
                    locationProcessor.stopUpdates()
                }
                val lat = intent?.getDoubleExtra("lat", 91.0) ?: 91.0
                val lon = intent?.getDoubleExtra("lon", 181.0) ?: 181.0

                if (lat < 90 && lon < 180) {
                    setLocation(lon, lat)
                }
            }
        }
    }

    private fun receiveLocation(loc: Location) {
       setLocation(loc.longitude, loc.latitude, true)
    }

    private fun setLocation(lon: Double, lat: Double) {
        setLocation(lon, lat, false)
    }

    private fun setLocation(lon: Double, lat: Double, gpsLocation: Boolean) {


        val p = Point(lon, lat)

        if (field == null) {
            field = GeomagneticField(lat.toFloat(), lon.toFloat(), p.z.toFloat(), System.currentTimeMillis())
        }


        if (gpsLocation) {
            lastLon = lon
            lastLat = lat
        }


        dataUpdate(p, gpsLocation)
    }

    private fun dataUpdate(p: Point, gpsLocation: Boolean) {

        if (!runningUpdate) {

            launch {

                try {
                    runningUpdate = true
                    var height = 1.0

                    var updateSuccess = true

                    val needNewData = integrator.needNewData(p)
                    if (needNewData) {
                        hud.setMessage("Loading data...")
                        withContext(Dispatchers.IO) {
                            updateSuccess = integrator.update(p)
                        }

                    }

                    if (updateSuccess && gpsLocation) {
                        withContext(Dispatchers.IO) {
                            height = integrator.getHeight(p)
                        }
                        p.z = height
                        status.height = height.toFloat()
                        openGLView.renderer.setCameraLocation(p)


                        if (needNewData) {


                            hud.setMessage("Loaded ok. Rendering data...")


                            val osm = integrator.getCurrentOSMTiles()
                            val hgt = integrator.getCurrentDEMTiles()
                            makePOIs(osm)

                            if (gpsLocation) {
                                withContext(Dispatchers.Default) {
                                    openGLView.renderer.setRenderData(osm, hgt)
                                }

                                hud.setMessage("Rendered ok. Calculating routing")
                                withContext(Dispatchers.IO) {
                                    signpostMgr.setDataset(osm)
                                }

                            } else {
                                locationProcessor.startUpdates()

                            }

                        }

                        var signs = listOf<Sign>()


                        withContext(Dispatchers.Default) {
                            signs = signpostMgr.update(p)
                        }


                        withContext(Dispatchers.IO) {
                            signs.forEach {
                                it.position.z = integrator.getHeight(it.position)
                            }
                        }

                        openGLView.renderer.renderSigns(signs)


                        //   openGLView.renderer.setHeight(height)
                        // TODO failed tiles
                    }

                } catch (e: OsmDemIntegrator.UpdateException) {
                    if (!job.isCancelled && System.currentTimeMillis() - lastNetworkErrorTime > 60000) {

                        AlertDialog.Builder(this@MainActivity)
                            .setMessage("There was a problem with loading the map data, probably due to no network connection. Hikar will try again shortly. Details: ${e.origException}")
                            .setPositiveButton("OK", null).show()
                        lastNetworkErrorTime = System.currentTimeMillis()

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    if (!job.isCancelled) {

                        AlertDialog.Builder(this@MainActivity)
                            .setMessage("An error occurred: $e.\nOccurred at latitude ${p.y}, longitude ${p.x}.\n\nTo help fix the bug, please report this info along with details of your device (model, Android version) at https://gitlab.com/nickw1/Hikar/issues, or email hikar.app@gmail.com")
                            .setPositiveButton("OK") { _, _ -> finish() }.show()
                    }
                } finally {
                    runningUpdate = false
                    hud.removeMessage()
                }
            }
        }
    }

    private fun receiveSensorInput(glR: FloatArray, bearingRadians: Float) {
        val orientation = FloatArray(3)
        val magNorth = field?.declination ?: 0.0f
        val actualAdjustment = magNorth + (if (enableOrientationAdjustment) orientationAdjustment else 0.0f)
        Matrix.rotateM(glR, 0, actualAdjustment, 0.0f, 1.0f, 0.0f)
        SensorManager.getOrientation(glR, orientation)
        openGLView.renderer.setOrientMtx(glR)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            0 -> {
                if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_DENIED || grantResults[1] == PackageManager.PERMISSION_DENIED || grantResults[2] == PackageManager.PERMISSION_DENIED)) {
                    permissionState = PermissionState.DENIED
                    AlertDialog.Builder(this)
                        .setMessage("Hikar will not work without camera, GPS and file permissions and will therefore now finish.")
                        .setPositiveButton("OK") { _, _ -> finish() }.show()
                } else {
                    permissionState = PermissionState.GRANTED
                    fotoapparat?.startWithTexCheck(openGLView)
                }
            }
        }
    }

    private fun makePOIs(osm: OsmDemIntegrator.KeyedOSMTiles) {
        Shared.pois.clear()
        val map = osm.tilesAsMap()
        map.entries.forEach { entry ->
            val dataset = entry.value as FreemapDataset
            dataset.operateOnPOIs { Shared.pois.add(it) }
        }
    }
}
