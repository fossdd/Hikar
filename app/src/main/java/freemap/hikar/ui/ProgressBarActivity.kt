package freemap.hikar.ui

import android.view.View
import android.widget.ProgressBar

abstract class ProgressBarActivity: CoroutineActivity() {
    abstract val progressBarId: Int

    protected fun showProgressBar(progress: Boolean) {
        findViewById<ProgressBar>(progressBarId).visibility = if(progress) View.VISIBLE else View.GONE
    }
}