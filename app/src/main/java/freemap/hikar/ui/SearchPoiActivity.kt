/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import freemap.data.POI
import freemap.hikar.R
import freemap.hikar.Shared
import kotlinx.android.synthetic.main.activity_search_poi.*

class SearchPoiActivity : ProgressBarActivity() {

    private val pois = Shared.pois.filter { !it.getValue("name").isNullOrBlank() }
    private var selectedPois = listOf<POI>()
    private var names = arrayListOf<String>()
    private var types = arrayListOf<String>()

    override val progressBarId = R.id.progressBarSearchPoi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_poi)

        poiListView.layoutManager = LinearLayoutManager(this)

        poiListView.adapter = PoiListAdapter(names, types) {
            showProgressBar(true)
            val intent = Intent()
            intent.putExtra("poi_id", selectedPois[it].getValue("osm_id").toLong())
            setResult(RESULT_OK, intent)
            finish()
        }

        onSearch()
    }

    fun onSearch(q: String = "") {

        selectedPois = pois.filter{ if (q.isBlank()) true else it.getValue("name")?.contains(q, true) ?: false }.sortedBy { it.getValue("name") }
        val names = selectedPois.map { it.getValue("name") }
        val types = selectedPois.map {
            val type = it.getValue("featuretype")
            if (type.isNullOrBlank()) "unknown" else type
        }
        (poiListView.adapter as PoiListAdapter).updateData(names, types)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_location_picker, menu)
        val item = menu?.findItem(R.id.locationPickerSearch)
        val searchViewMap = item?.actionView as SearchView
        searchViewMap.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(q: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(q: String?): Boolean {
                onSearch(q ?: "")
                searchViewMap.clearFocus()
                return true
            }
        })

        return true
    }

}