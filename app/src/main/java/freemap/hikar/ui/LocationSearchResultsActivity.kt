/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import freemap.hikar.R
import kotlinx.android.synthetic.main.activity_search_poi.*
import org.json.JSONArray

class LocationSearchResultsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_poi)


        poiListView.layoutManager = LinearLayoutManager(this)

        val json = JSONArray(intent.getStringExtra("json"))
        val names = arrayListOf<String>()
        val types = arrayListOf<String>()


        for (i in 0 until json.length()) {
            val nameComponents = json.getJSONObject(i).getString("display_name").split(",")
            names.add(nameComponents[0])
            types.add(
                "${json.getJSONObject(i).getString("type")} (${nameComponents.subList(
                    1,
                    nameComponents.size - 1
                ).joinToString(",") { it.trim() }})"
            )
        }
        poiListView.adapter = PoiListAdapter(names, types) {
            val intent = Intent()
            intent.putExtra("lat", json.getJSONObject(it).getDouble("lat"))
            intent.putExtra("lon", json.getJSONObject(it).getDouble("lon"))
            setResult(RESULT_OK, intent)
            finish()
        }

    }
}