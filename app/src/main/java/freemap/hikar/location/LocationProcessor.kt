/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.content.ContextCompat

class LocationProcessor(private val ctx: Context, private val time: Long, private val distance: Float, val onReceiveLocation: (Location)->Unit): LocationListener {



    private val mgr =  ctx.getSystemService(Context.LOCATION_SERVICE) as LocationManager


    fun startUpdates() {
        if(ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, distance, this)
        }
    }

    fun stopUpdates() {
        mgr.removeUpdates(this)
    }

    override fun onLocationChanged(loc: Location) {
        onReceiveLocation(loc)
    }

    override fun onProviderEnabled(provider: String) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

    }

    fun isProviderEnabled(): Boolean {
        return mgr.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

}