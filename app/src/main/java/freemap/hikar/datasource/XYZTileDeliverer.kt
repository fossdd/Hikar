/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.datasource

import android.os.Environment
import freemap.data.GoogleProjection
import freemap.data.Point
import freemap.datasource.TiledData
import java.io.File
import java.lang.RuntimeException

class XYZTileDeliverer(private val dataSource: TiledDataSource, private val layer: String, var zoom: Int) {

    private val proj = GoogleProjection()
    private val path = "${Environment.getExternalStorageDirectory().absolutePath}/hikar/xyz"
    private val tiles = hashMapOf<String, TiledData?>()
    private val lastRequestedTile = GoogleProjection.Tile(0, 0, 0)


    init {
        val file = File(path)
        if(!file.exists()) {
            file.mkdirs()
        }
    }

    class TiledDataInfo(var tiledData: HashMap<String, TiledData?>, val zoomLevel: Int, val xCentre: Int, val yCentre: Int, val dimension: Int) {
        constructor(): this(HashMap<String,TiledData?>(), 0, 0, 0, 0)
    }

    abstract class TiledDataSource(private val webPath: String) {
        abstract fun loadFromFile(f: File, t: GoogleProjection.Tile) : TiledData
        fun loadFromWeb(t: GoogleProjection.Tile, cacheFile: File): TiledData? {
            return doLoadFromWeb(webPath.replace("{x}", t.x.toString()).replace("{y}", t.y.toString()).replace("{z}", t.z.toString()), t, cacheFile)
        }

        abstract fun doLoadFromWeb(url: String, t: GoogleProjection.Tile, cacheFile: File) : TiledData?
    }

    fun getCurrentTile(lonLat: Point): TiledData? {
        return getData(proj.getTileFromLonLat(lonLat, zoom),1, false).tiledData.toList()[0].second
    }

    fun getData(lonLat: Point, n:Int = 1, cache: Boolean = true) : TiledDataInfo {
        return getData(proj.getTileFromLonLat(lonLat, zoom), n, cache)
    }

    // get multiple tiles (e.g. current tile and surrounding tiles)
    // n is the dimension of the tile group to obtain (e.g. 3 for 3x3=9 tiles total)
    private fun getData(t: GoogleProjection.Tile, n:Int = 1, cache: Boolean = true) : TiledDataInfo {
        val tileset = hashMapOf<String, TiledData?>()
        val t1 = GoogleProjection.Tile(0,0, t.z)
        val n1 = n/2
        for (x in t.x - n1..t.x + n1) {
            for (y in t.y - n1..t.y + n1) {
                t1.x = x
                t1.y = y
                tileset["$zoom/$x/$y"] = doGetData(t1, cache)
            }
        }
        return TiledDataInfo(tileset, zoom, t.x, t.y, n)
    }

    // loads data corresponding to an xyz tile
    // will use the cached version if it exists, if not retrieve fom web and cache it
    private fun doGetData(t: GoogleProjection.Tile, cache: Boolean=true): TiledData? {
        val key = "$zoom/${t.x}/${t.y}"

        if (!tiles.containsKey(key)) {

            val dir = File("$path/$layer/$zoom/${t.x}/")

            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    throw RuntimeException("Unable to make directory for cache: attempted dir $path/$layer/$zoom/${t.x}/")
                }
            }

            val file = File("$dir/${t.y}")
            if (file.exists()) {
                tiles[key] = dataSource.loadFromFile(file, t)
            } else {
                tiles[key] = dataSource.loadFromWeb (t, file)
            }
        }
        return tiles[key]
    }

    fun needNewData(lonLat: Point): Boolean {
        val t = proj.getTileFromLonLat(lonLat, zoom)
        if(t.x != lastRequestedTile.x || t.y != lastRequestedTile.y || t.z != lastRequestedTile.z) {
            lastRequestedTile.apply {
                x = t.x
                y = t.y
                z = t.z
            }
            return true
        }
        return false
    }

    fun deleteData() {
        File("$path/$layer").deleteRecursively()
    }
}