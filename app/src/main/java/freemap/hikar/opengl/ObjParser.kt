/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.NullPointerException


object ObjParser {


    fun parseObj(inp: InputStream): Model? {
        try {
            val vertices = arrayListOf<Float>()
            val texCoords = arrayListOf<Float>()
            val vtxIndices = arrayListOf<Short>()
            val reader = BufferedReader(InputStreamReader(inp))
            var line: String?
            val vtCombinations = arrayListOf<String>()


            line = reader.readLine()
            while (line != null) {
                if (line.isNotEmpty()) {

                    val components = line.split("\\s+".toRegex())

                    if (components.size >= 2) {

                        when (components[0]) {
                            "v" -> {
                                //         Log.d("hikar", "Is a vertex")
                                if (components.size == 4) {
                                    // in the obj, y is vertical (needs to be z) and
                                    // z is to the right (needs to be x)
                                    vertices.add(components[3].toFloat()) // z->x
                                    vertices.add(components[1].toFloat()) // x->y
                                    vertices.add(components[2].toFloat()) // y->z
                                }
                            }

                            "vt" -> {

                                //        Log.d("hikar", "Is a tex")
                                if (components.size >= 3) {
                                    //      Log.d("hikar", "Adding texcoords");
                                    for (j in 1..2) {
                                        texCoords.add(components[j].toFloat())
                                    }
                                }

                            }
                            "f" -> {
                                //             Log.d("hikar", "LINE IS $line, Components size is : ${components.size} Components joined=${components.joinToString(",")}")

                                when (components.size) {
                                    4 -> {
                                        //                     Log.d("hikar", "Triangle");
                                        for (j in 1..3) {
                                            val vtn = components[j].split("/".toRegex())
                                            if (vtn.size == 3) {
                                                val vtxIndex = Integer.parseInt(vtn[0]) - 1
                                                val texIndex = Integer.parseInt(vtn[1]) - 1
                                                val key = "$vtxIndex/$texIndex"
                                                val i = vtCombinations.indexOf(key)
                                                if (i == -1) {
                                                    vtxIndices.add(vtCombinations.size.toShort())
                                                    vtCombinations.add(key)
                                                } else {
                                                    vtxIndices.add(i.toShort())
                                                }

                                            }
                                        }
                                    }

                                    5 -> {
                                        //       Log.d("hikar", "Quad - splitting");
                                        val vIndices = ShortArray(4)
                                        for (j in 1..4) {
                                            //        Log.d("hikar", "Component: " + components[j]);
                                            val vtn = components[j].split("/".toRegex())
                                            if (vtn.size == 3) {
                                                val vtxIndex = Integer.parseInt(vtn[0]) - 1
                                                val texIndex = Integer.parseInt(vtn[1]) - 1
                                                check (!(vtxIndex < 0 || texIndex< 0)) { "HELP! Indices cannot be less than 0" }
                                                val key = "$vtxIndex/$texIndex"
                                                val i = vtCombinations.indexOf(key)
                                                if (i == -1) {
                                                    vIndices[j - 1] = vtCombinations.size.toShort()
                                                    vtCombinations.add(key)
                                                } else {
                                                    vIndices[j - 1] = i.toShort()
                                                }
                                                //          Log.d("hikar", "For key " + key + " index is " + vIndices[j - 1]);
                                                //     Log.d("hikar", "Adding vtxIndex " + vtxIndex + " and texIndex " + texIndex);
                                            }
                                        }
                                        vtxIndices.add(vIndices[0])
                                        vtxIndices.add(vIndices[1])
                                        vtxIndices.add(vIndices[2])
                                        vtxIndices.add(vIndices[2])
                                        vtxIndices.add(vIndices[3])
                                        vtxIndices.add(vIndices[0])
                                        //  Log.d("hikar", "Adding indices " + vIndices[0] + " " + vIndices[1] + " " + vIndices[2] + " " + vIndices[2] + " " + vIndices[3] + " " + vIndices[0);
                                    }

                                    !in 2..3 -> {
                                        // if more than 4 vertices defined, do a triangle fan
                                        //   Log.d("hikar", "Triangle fan");
                                        val vIndices = ShortArray(components.size - 1)
                                        for (j in 1 until components.size) {

                                            val vtn = components[j].split("/".toRegex())
                                            if (vtn.size == 3) {
                                                val vtxIndex = (vtn[0].toInt()) - 1
                                                val texIndex = (vtn[1].toInt()) - 1
                                                val key = "$vtxIndex/$texIndex"
                                                val i = vtCombinations.indexOf(key)
                                                if (i == -1) {
                                                    vIndices[j - 1] = vtCombinations.size.toShort()
                                                    vtCombinations.add(key)
                                                } else {
                                                    vIndices[j - 1] = i.toShort()
                                                }
                                                //     Log.d("hikar", "For key " + key + " index is " + vIndices[j - 1]);
                                            }
                                        }

                                        for (j in 1 until components.size - 2) {
                                            vtxIndices.add(vIndices[0])
                                            vtxIndices.add(vIndices[j])
                                            vtxIndices.add(vIndices[j + 1])
                                            //    Log.d("hikar", "Triangle fan adding indices " + vIndices[0] + " " + vIndices[j] + " " + vIndices[j + 1]);
                                        }
                                        vtxIndices.add(vIndices[0])
                                        vtxIndices.add(vIndices[components.size - 2])
                                        vtxIndices.add(vIndices[1])
                                        //     Log.d("hikar", "Triangle fan adding indices " + vIndices[0] + " " + vIndices[components.size - 2] + " " + vIndices[1]);
                                    }

                                }
                            }
                        }
                    }
                }
                line = reader.readLine()
            }

            val recordSize = 5
            val vtxArray = FloatArray(vtCombinations.size * recordSize)
            val idxArray = ShortArray(vtxIndices.size)

            var i = 0
            vtCombinations.forEach {

                //     Log.d("hikar", "Adding vtx/tex combination: $it")
                val vt = it.split("/".toRegex())
                val vIdx = vt[0].toInt()
                val tIdx = vt[1].toInt()

                vtxArray[i * recordSize] = vertices[vIdx * 3]
                vtxArray[i * recordSize + 1] = vertices[vIdx * 3 + 1]
                vtxArray[i * recordSize + 2] = vertices[vIdx * 3 + 2]
                vtxArray[i * recordSize + 3] = texCoords[tIdx * 2]
                vtxArray[i * recordSize + 4] = texCoords[tIdx * 2 + 1]
                i++
            }

            i = 0
            vtxIndices.forEach {
                idxArray[i++] = it
            }
            //   Log.d("hikar", "vtxArray size=" + vtCombinations.size * 5 + " indices size=" + vtxIndices.size + " vertices size=" + vertices.size + " texcoords size=" + texCoords.size);
            return Model(vtxArray, idxArray)
        } catch (e: NullPointerException) {
            //    Log.d("hikar", "NullPointerException loading model: " + e);
            e.printStackTrace()
            return null
        }
    }
}
