/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import android.content.Context
import freemap.data.Point
import freemap.hikar.signposting.Sign


class SignpostModelMaker(private val postAsset: String, private val overlayGpu: TextureGPUInterface) {


    fun make(ctx: Context, sign: Sign, loc: Point): CompoundModel {
        val compoundModel = OverlayCompoundModel(loc.x.toFloat(), loc.y.toFloat(), loc.z.toFloat(), overlayGpu)
        val model = ObjParser.parseObj(ctx.assets.open(postAsset))
        if (model != null) {
            compoundModel.addModel(model, 0.0f, 0.0f)

            var i = 0
            sign.getSignPlates().forEach {

                // convert standard bearings to opengl angles (x=east=0, increase anticlockwise)
                var openglRotationAngle = -(it.getAngle() - 90).toFloat()
                openglRotationAngle = if (openglRotationAngle < -180) 360 + openglRotationAngle else
                    (if (openglRotationAngle > 180) -360 + openglRotationAngle else openglRotationAngle)
                val model1 =
                    ObjParser.parseObj(ctx.assets.open(it.getSignAsset()))
                if (model1 != null) {
                    compoundModel.addModel(
                        model1,
                        openglRotationAngle,
                        it.getText(),
                        it.getModelTextOverlay(),
                        sign.getVerticalDisplacement(i)
                    )

                }
                i++
            }
        }
        return compoundModel
    }
}
