/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import android.opengl.GLES20


import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

class ModelTextOverlay(
    private var armLeft: Float,
    private var armTop: Float,
    private var armRight: Float,
    private var armBottom: Float,
    private var front: Float,
    private var back: Float,
    margin: Float,
    private val nSides: Int
) {

    private var vBuf: FloatBuffer? = null
    private var iBuf: ShortBuffer? = null
    private var text = arrayOf("")
    private var scale = 1.0f
    private val xCharPx = 32
    private val yCharPx = 32
    private val nCols = 32 // covers up to Cyrillic in Unicode

    init {
        armLeft += margin
        armTop -= margin
        armRight -= margin
        armBottom += margin
    }


    fun setText(text: Array<String>) {
        this.text = text
    }


    fun scale(scale: Float) {
        this.scale = scale
    }

    fun genBuffers() {
        var nChars = 0
        val nRows = 39

        text.forEach {
            nChars += it.length
        }


        val vertices = FloatArray(nChars * 20 * nSides)
        val coord = FloatArray(2)

        armLeft *= scale
        armTop *= scale
        armRight *= scale
        armBottom *= scale
        front *= scale
        back *= scale


        val textHeight = (armTop - armBottom) / text.size
        var charCount = 0
        for (destIdx in 0 until text.size) {
            val textWidth = (armRight - armLeft) / text[destIdx].length

            for (chrIdx in 0 until text[destIdx].length) {

                getCoord(text[destIdx][chrIdx], coord)

                vertices[(chrIdx + charCount) * 20] = armLeft + textWidth * chrIdx
                vertices[(chrIdx + charCount) * 20 + 15] = armLeft + textWidth * chrIdx
                vertices[(chrIdx + charCount) * 20 + 1] = front
                vertices[(chrIdx + charCount) * 20 + 6] = front
                vertices[(chrIdx + charCount) * 20 + 11] = front
                vertices[(chrIdx + charCount) * 20 + 16] = front
                vertices[(chrIdx + charCount) * 20 + 2] = armTop - (destIdx + 0.9f) * textHeight
                vertices[(chrIdx + charCount) * 20 + 7] = armTop - (destIdx + 0.9f) * textHeight
                vertices[(chrIdx + charCount) * 20 + 3] = coord[0] / (xCharPx * nCols)
                vertices[(chrIdx + charCount) * 20 + 18] = coord[0] / (xCharPx * nCols)
                vertices[(chrIdx + charCount) * 20 + 4] = 1 - (coord[1] + yCharPx) / (yCharPx * nRows)
                vertices[(chrIdx + charCount) * 20 + 9] = 1 - (coord[1] + yCharPx) / (yCharPx * nRows)

                vertices[(chrIdx + charCount) * 20 + 5] = armLeft + textWidth * (chrIdx + 1)
                vertices[(chrIdx + charCount) * 20 + 10] = armLeft + textWidth * (chrIdx + 1)
                vertices[(chrIdx + charCount) * 20 + 8] = (coord[0] + xCharPx) / (xCharPx * nCols)
                vertices[(chrIdx + charCount) * 20 + 13] = (coord[0] + xCharPx) / (xCharPx * nCols)

                vertices[(chrIdx + charCount) * 20 + 12] = armTop - destIdx * textHeight
                vertices[(chrIdx + charCount) * 20 + 17] = armTop - destIdx * textHeight
                vertices[(chrIdx + charCount) * 20 + 14] = 1 - coord[1] / (yCharPx * nRows)
                vertices[(chrIdx + charCount) * 20 + 19] = 1 - coord[1] / (yCharPx * nRows)

            }


            // Back of the sign - but don't always render text on back (e.g. noticeboard)
            for (chrIdx in text[destIdx].length until text[destIdx].length * nSides) {
                for (j in 1 until 20) {
                    vertices[(chrIdx + charCount) * 20 + j] =
                        vertices[(chrIdx + charCount - text[destIdx].length) * 20 + j]
                }

                vertices[(chrIdx + charCount) * 20] = armRight - textWidth * (chrIdx - text[destIdx].length)
                vertices[(chrIdx + charCount) * 20 + 5] = armRight - textWidth * ((chrIdx + 1) - text[destIdx].length)
                vertices[(chrIdx + charCount) * 20 + 10] = armRight - textWidth * ((chrIdx + 1) - text[destIdx].length)
                vertices[(chrIdx + charCount) * 20 + 15] = armRight - textWidth * (chrIdx - text[destIdx].length)
                vertices[(chrIdx + charCount) * 20 + 1] = back
                vertices[(chrIdx + charCount) * 20 + 6] = back
                vertices[(chrIdx + charCount) * 20 + 11] = back
                vertices[(chrIdx + charCount) * 20 + 16] = back
            }

            charCount += text[destIdx].length * nSides
        }

        val buf = ByteBuffer.allocateDirect(vertices.size * 4)
        buf.order(ByteOrder.nativeOrder())
        vBuf = buf.asFloatBuffer()
        vBuf?.position(0)
        vBuf?.put(vertices)

        val indices = ShortArray(nChars * 6 * nSides)
        for (i in 0 until nChars * nSides) {
            indices[i * 6] = (i * 4).toShort()
            indices[i * 6 + 1] = (i * 4 + 1).toShort()
            indices[i * 6 + 2] = (i * 4 + 2).toShort()
            indices[i * 6 + 3] = (i * 4 + 2).toShort()
            indices[i * 6 + 4] = (i * 4 + 3).toShort()
            indices[i * 6 + 5] = (i * 4).toShort()
        }

        val buf1 = ByteBuffer.allocateDirect(indices.size * 2)
        buf1.order(ByteOrder.nativeOrder())
        iBuf = buf1.asShortBuffer()
        iBuf?.position(0)
        iBuf?.put(indices)
    }

    fun draw(gpu: GPUInterface) {
        gpu.drawTexturedBufferedData(vBuf!!, iBuf!!, "aVertex", "aTexCoord", GLES20.GL_TRIANGLES)
    }

    private fun getCoord(c: Char, coords: FloatArray) {
        val index = c.toInt() - 32
        coords[0] = (xCharPx * (index % nCols)).toFloat()
        coords[1] = (yCharPx * (index / nCols)).toFloat()
    }
}

