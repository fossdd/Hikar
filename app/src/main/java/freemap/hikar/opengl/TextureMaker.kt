/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.opengl

import android.content.Context
import android.graphics.BitmapFactory
import android.opengl.GLES20
import android.opengl.GLUtils

class TextureMaker(private val ctx: Context) {

    fun makeTextureGPUInterface(
        texFile: String,
        vertexShader: String,
        fragmentShader: String,
        textureUnit: Int
    ): TextureGPUInterface{
        val textureId = makeTexture(texFile)

        if (textureId != 0) {

            return TextureGPUInterface(
                "texture",
                vertexShader,
                fragmentShader,
                textureId,
                textureUnit
            ).apply {
                setUniform1i("uTexture", textureUnit - GLES20.GL_TEXTURE0)
            }
        } else {
            throw Exception("Could not create texture from $texFile")
        }
    }

    private fun makeTexture(texFile: String): Int {
        val textureId = IntArray(1)

        GLES20.glGenTextures(1, textureId, 0)
        if (textureId[0] != 0) {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId[0])
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)
            val options = BitmapFactory.Options().apply { inScaled = false }
            val istr = ctx.assets.open(texFile)
            val bmp = BitmapFactory.decodeStream(istr, null, options)
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bmp, 0)
            bmp?.recycle()
        }
        return textureId[0]
    }
}