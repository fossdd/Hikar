/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.signposting

import freemap.data.Algorithms
import freemap.data.Point

class JunctionManager(private val graph: Graph) {

    private var lastJunctionPoint: Point? = null
    private val juncChangeThreshold = 5.0 // 220319 changed at same time as additional test below

    fun getJunction(p: Point, juncThreshold: Double = 100.0): Junction? {

        if (lastJunctionPoint == null || Algorithms.haversineDist(
                p.x,
                p.y,
                lastJunctionPoint?.x ?: 0.0,
                lastJunctionPoint?.y ?: 0.0
            ) > juncChangeThreshold
        ) {
            var nearestDist = Double.MAX_VALUE
            var selectedNode: Vertex? = null

            //    Log.d("hikar", "getJunction(): test point=$p")
            graph.vertices.forEach {
                if (!it.poiVertex) {
                    val curDist = Algorithms.haversineDist(it.p.x, it.p.y, p.x, p.y)


                    // 220319 If the current junction is nearer than the last, select it

                    if (curDist < juncThreshold && curDist < nearestDist) {
                        nearestDist = curDist
                        selectedNode = it
                    }
                }
            }

            if (selectedNode != null && selectedNode!!.p != lastJunctionPoint) {

                val junction = Junction(selectedNode!!.p)

                // we've found a node...
                selectedNode!!.edges.filter { e -> e.data.isWalkingRoute }.forEach { e ->
                    junction.addWayInfo(e.data.localIdWay.way.tags, e.getInitialBearing())
                }
                lastJunctionPoint = selectedNode!!.p
                return if (junction.ways.isEmpty()) null else junction
            }

        }
        return null
    }
}