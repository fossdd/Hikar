package freemap.hikar.signposting

import freemap.data.Point

class Junction(val point: Point){
    data class WayInfo(val tags: HashMap<String, String>, val bearing: Double)

    val ways = arrayListOf<WayInfo>()

    val id: String
        get() = ways.map {  it.tags["osm_id"] }.joinToString (",")

    fun addWayInfo(tags: HashMap<String, String>, bearing: Double) {
        ways.add(WayInfo(tags, bearing))
    }
}