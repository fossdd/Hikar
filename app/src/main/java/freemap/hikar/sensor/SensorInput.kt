/*
    Copyright 2013-19 Nick Whitelegg hikar.app@gmail.com

    This file is part of Hikar.

    Hikar is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hikar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with Hikar.  If not, see <http://www.gnu.org/licenses/>.
 */

package freemap.hikar.sensor

import android.hardware.SensorManager
import android.hardware.Sensor
import android.hardware.SensorEventListener


import android.hardware.SensorEvent
import android.view.Surface
import android.view.WindowManager

import android.content.Context
import kotlin.math.PI
import kotlin.math.abs

class SensorInput(private val receiver: (FloatArray, Float)->Unit) : SensorEventListener {
    private var ctx: Context? = null
    private var accelerometer: Sensor? = null
    private var magneticField: Sensor? = null
    private val accelValues = FloatArray(3)
    private val magValues = FloatArray(3)
    private var k = 0.1f
    private val prevOrientation = FloatArray(3)


    fun start(): Boolean {
        val sMgr = ctx?.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
        accelerometer = sMgr?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        magneticField = sMgr?.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)

        if (accelerometer != null && magneticField != null) {
            sMgr?.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI)
            sMgr?.registerListener(this, magneticField, SensorManager.SENSOR_DELAY_UI)
            return true
        }
        return false
    }

    fun stop() {
        val sMgr = ctx?.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
        sMgr?.unregisterListener(this, accelerometer)
        sMgr?.unregisterListener(this, magneticField)

    }

    fun detach() {
        ctx = null
    }

    fun attach(ctx: Context) {
        this.ctx = ctx
    }

    override fun onAccuracyChanged(sensor: Sensor, acc: Int) {

    }

    // Two examples (devx.com article and Photos Around)
    // recommend using an exponential smoothing method
    // on the data with a factor of 0.05 or 0.075.

    override fun onSensorChanged(ev: SensorEvent) {
        val rotMtx = FloatArray(16)
        val inclMtx = FloatArray(16)
        val glR = FloatArray(16)
        val orientation = FloatArray(3)
        var orientAdj = 0.0f
        when (ev.sensor.type) {
            Sensor.TYPE_ACCELEROMETER -> {

                for (i in 0 until 3) {
                    accelValues[i] = (1 - k) * accelValues[i] + k * ev.values[i]
                }
            }

            Sensor.TYPE_MAGNETIC_FIELD -> {

                for (i in 0 until 3) {
                    magValues[i] = (1 - k) * magValues[i] + k * ev.values[i]
                }
            }
        }

        if (ctx != null) {
            SensorManager.getRotationMatrix(rotMtx, inclMtx, accelValues, magValues)

            SensorManager.getOrientation(rotMtx, orientation)


            val wm = ctx?.getSystemService(Context.WINDOW_SERVICE) as WindowManager?
            if (wm != null) {
                when (wm.defaultDisplay.rotation) {
                    Surface.ROTATION_90 -> {

                        SensorManager.remapCoordinateSystem(rotMtx, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, glR)
                        orientAdj = (0.5 * PI).toFloat()
                    }


                    Surface.ROTATION_180 -> {
                        SensorManager.remapCoordinateSystem(
                            rotMtx,
                            SensorManager.AXIS_MINUS_X,
                            SensorManager.AXIS_MINUS_Y,
                            glR
                        )
                        orientAdj = PI.toFloat()
                    }

                    Surface.ROTATION_270 -> {
                        SensorManager.remapCoordinateSystem(rotMtx, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, glR)
                        orientAdj = (-0.5 * PI).toFloat()
                    }

                    Surface.ROTATION_0 -> {
                        System.arraycopy(rotMtx, 0, glR, 0, 16)
                    }

                }
            }
            var doReceiveSensorInput = false
            val angleChangeThreshold = 2.0f * (PI.toFloat() / 180.0f)

            for (i in 0 until 3) {
                if (abs(orientation[i] - prevOrientation[i]) >= angleChangeThreshold) {
                    doReceiveSensorInput = true
                    break
                }
            }


            if (doReceiveSensorInput) {

                var realBearing = orientation[0] + orientAdj
                val fPi = PI.toFloat()
                realBearing = if (realBearing > fPi) realBearing - fPi * 2 else
                    (if (realBearing < -fPi) realBearing + fPi * 2 else realBearing)
                receiver(glR, realBearing)
                System.arraycopy(orientation, 0, prevOrientation, 0, 3)

            }
        }
    }
}