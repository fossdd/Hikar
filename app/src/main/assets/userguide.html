 <!DOCTYPE html>
<html>
<head>
    <title>Hikar - OpenStreetMap Augmented Reality for Walkers and Hikers - v0.2</title>
</head>
<body>
<h1>Hikar 0.3 - OpenStreetMap Augmented Reality for Walkers and Hikers</h1>
<hr/>
<div id='content'>
    <p>
        Hikar
        (see <a href='https://gitlab.com/nickw1/Hikar'>
        gitlab repository</a>) is an augmented reality app for Android (4.2+) which
        overlays <strong>OpenStreetMap footpaths, hiking trails and roads</strong>
        and <strong>virtual signposts</strong> on the device's camera feed. Its aim is to help navigation
        for walkers/hikers
        (hence the name) and other outdoor users throughout Europe. For example,
        imagine you are entering a large field and it's not clear where the exit is.
        Hikar will overlay the course of the footpath on your phone's camera feed
        helping you to navigate across the field. Or, you're at a junction of paths
        and it's not clear which is the correct way.
        Hikar will show you a virtual signpost indicating the route to nearby points of interest, such as villages,
        peaks or pubs. Or, you're having to make your way across a pathless moor. Again, Hikar can help you find the
        way.</p>
    <p>The colour scheme used is based on OpenStreetMap
        tagging and is as follows:</p>
    <ul>
        <li>green = footpath</li>
        <li>brown = bridleway</li>
        <li>orange = track</li>
        <li>light blue = cycleway</li>
        <li>red = byway</li>
        <li>white = road, or any other type of route</li>
    </ul>

    <h2>What's new in version 0.3?</h2>
    <p>Hikar 0.3 offers a few enhancements over 0.2, namely:</p>
    <ul>
        <li>Larger, easier to read signposts</li>
        <li><strong>Noticeboards</strong> - see below</li>
        <li><strong>Map-based interface</strong> for selecting data pre-download location. You can also search
            for a location to download data using Nominatim.
        </li>
        <li><strong>Calculate route to a nearby POI.</strong> You can now calculate and display the exact route to a
            nearby
            point of interest.
        </li>
        <li>Selected POIs on signposts are accompanied by icons; notably railway stations, pubs, and cafes.</li>
        <li>Different zoom levels to download varying amounts of data, some optimised for countryside use, others
        for urban use.</li>
    </ul>
    <h2>Using Hikar</h2>
    <h3>Where can I use Hikar?</h3>
    <p>Currently Hikar works in the whole of <strong>Europe, and Turkey</strong>; the Geofabrik
        Europe planet extract is used so anywhere in this extract will be covered.
        Other parts of the world are currently not available though may be in the future.</p>
    <h3>Downloading data</h3>
    <p>When Hikar starts, you will see the camera feed appear.
        Data will then be downloaded from the Hikar server and then saved on the device,
        so that <strong>the second time you are in the area, Hikar will work fully offline.</strong></p>
    <p>If the download
        fails (e.g. no network connection), you will get an error and the app will
        try again when your GPS position changes. You can also <strong>pre-download data</strong> for a
        certain area before you go out (to avoid
        relying on poor network connectivity in the countryside); to
        do this select the "Map / pre-download data at a given location" menu option. A map-based interface will appear;
        you
        can select a location by pressing the map or by searching for a location in the search bar (OSM Nominatim is
        used
        as the search provider). An area of (in the default zoom level) around 9x9km containing
        this latitude and longitude will be downloaded.</p>
    <h4>Data download zoom levels - optimisation for rural and urban areas</h4>
    <p>New in 0.3 you can <strong>change the data download zoom level</strong> via the Settings. The default level,
        which downloads around 9km x 9km data,
    is ideal for countryside and small town use, however it is slow to generate signposts in cities.
        Consequently, there is also an "urban" mode, which downloads only around 4.5km x 4.5km
    of data but is faster to generate signposts, a "large urban area" for big cities (around 2.25km x 2.25km) and
    a "sparsely populated countryside mode" for countryside areas with few large settlements (downloads around
    18km x 18km data; usable in the New Forest, for instance).</p>
    <h3>Zooming in and out</h3>
    <p>On devices running Android Lollipop (5.0) and later, Hikar will automatically work out the horizontal field of
        view of your camera. However if the field of view is inaccurate you may
        change it by pinching the screen in and out. The
        new field of view will appear on the display.</p>
    <p>
        You can also pinch the screen <strong>to zoom in if you are unable to read a signpost</strong>.
        The status bar will show you the adjustment relative to the normal field of view, so you know which
        value to reset it too once you've finished zooming in.
    </p>
    <h3>Routing to a nearby POI</h3>
    <p>Hikar 0.3 allows you to find the route to a nearby point of interest, such as a particular pub or village.
        Select the "Route to given POI" menu option, and then pick or search for your chosen POI.
        The route will then be shown as a yellow overlay on the AR view of the
        OSM map data - so just "follow the yellow brick road" to your destination!</p>

    <h3>Virtual noticeboards</h3>
    <p>New in 0.3 are <em>virtual noticeboards</em>, showing issues or places of interest as augmented reality
        noticeboards.
        These might include, on the one hand, hazards
        such as muddy or
        blocked paths, steep hills, natural hazards or aggressive cows - and on the other hand, features of historical
        or natural interest such as ancient monuments, nice views, or unusual plant species. </p>
    <h4>Viewing existing noticeboards</h4>
    <p>If a noticeboard has already been added by a user (see below) you will be able to view details of the
        issue at the <em>path junctions</em> at either ends of the path containing the noticeboard.
        So, for example, you might see "Aggressive cows 200m ahead" or "Nice view 500m ahead". A further noticeboard
        is shown at the exact location of the issue (or place of interest). Up to two noticeboards showing issues or
        places of interest on a given path are shown.</p>
    <h4>Adding new noticeboards</h4>
    <p>The noticeboards are intended to be crowd-sources. So any user can add
        brand-new noticeboards, either in-the-field with the <em>add noticeboard</em> menu option, or via the <a
                href='https://hikar.org'>
            hikar.org
        </a> website. Note that you'll need to sign up for an account at hikar.org before doing this, and specify
        your username and password in the preferences. A word of caution; your password is currently stored
        unencrypted on the device, though it is transferred over to the server with TLS encryption (HTTPS).</p>
    <h2>Current status</h2>

    <p>Hikar should currently work throughout Europe, with signposts being displayed in the local language
        (or, if you specify in the Settings, English if it's available) in all areas which use Roman, Greek or Cyrillic
        characters.
        However there are a few issues:
    </p>
    <ul>
        <li>Unlikely to work very well in very large cities (London, Paris etc) due to the sheer volume of data; the app
            is primarily aimed at, and optimised for, use in the countryside or smaller towns and cities. Based on a
            test in London, the basic AR
            functionality of overlaying the paths and roads on the camera feed is likely to work but it will take
            several minutes to generate
            signposts.
        </li>
        <li>Uses a lot of battery power - perhaps inevitable for an AR app.
            Therefore, ensure your battery is fully
            charged before setting out; dimming the screen and turning off mobile data are
            also supposed to help. (If doing the latter, you'll need to ensure you download
            data for the area you are walking before you set out!)
        </li>
        <li>GPS inaccuracy (both the GPS receiver on the device, and slight inaccuracies in the OSM data)
            leading to the OSM way being slightly offset from its true position, so do not expect the computer
            generated path to <em>perfectly</em> match its course on the ground. "Pixel-perfect" AR is, however,
            the long-term plan; see below.
        </li>
        <li>Occasional compass inaccuracies. Moving your device in a figure-of-eight pattern will help.</li>
        <li>Computer vision algorithms are currently not implemented, so consequently "pixel-perfect" AR is
            currently not supported. So signposts, for example, may change position slightly due to GPS and sensor
            inaccuracies, and paths may "float" above the ground slightly. The plan moving forward is to research
            and apply computer vision techniques to attempt to achieve pixel-perfect AR to allow, for example,
            a signpost to always appear at the same apparent position on the ground irrespective of changes
            in sensor or GPS readings. This was originally the plan for 0.3, but then I decided that there
            were enough enhancements already implemented to bump the version up from 0.2.x to 0.3.0.
        </li>
        <li>For currently unknown reasons <strong>it appears not to work on Amazon Fire HD devices.</strong></li>
    </ul>


    <h2>Bug reporting</h2>

    <p>The app has been extensively tested, however it is always possible that a bug may occur, particularly as
        there are a large number of Android devices out there (and obviously not all have been tested!), or there
        is something unusual about the path data in a particular location.
        With most bugs, the app will stop with a message about the bug and will display the current latitude and
        longitude (which can help the developer investigate its cause). If a bug occurs please note down all information
        shown and <em>report an issue at https://gitlab.com/nickw1/Hikar/issues</em> or
        <em>email the developer</em>, Nick Whitelegg on hikar.app@gmail.com.</p>

    <h2>Obtaining the source code</h2>

    <p>Source code is available
        <a href='https://gitlab.com/nickw1/Hikar'>here</a>.
    </p>

    <h2>Disclaimer</h2>
    <p>Please <strong>do not rely on Hikar for safe navigation in hazardous areas</strong>, e.g. mountains and
        cliffs. Small inaccuracies in the GPS receiver or surveyed data could result in a path sending you over
        a sheer rock face if you are using no other means of orientation. As always, <strong>use common sense and
            experience when
            hiking
            in the hills!</strong></p>
</div>

</body>
</html>



