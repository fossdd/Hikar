# Hikar 0.3.1

Hikar is an **augmented reality** app for Android (4.2+) which overlays footpaths
from OpenStreetMap on the device's camera feed and shows virtual signposts,
helping you navigate.

**Get it here:** https://play.google.com/store/apps/details?id=freemap.hikar

Its aim is to help navigation for walkers/hikers (hence the name) and other
outdoor users. For example, imagine you are entering a large field and it's
not clear where the exit is. Hikar will overlay the course of the footpath on
your phone's camera feed helping you to navigate across the field. Or, you're
at a junction of paths and it's not clear which is the correct way. Hikar will
show a virtual signpost, showing the direction and distance of nearby POIs
such as villages, hills, mountains or pubs. Or, you're having to make your way
across a pathless moor. Again, Hikar will help you find the way.

From version 0.2, Hikar covers the whole of Europe, plus Turkey (0.1 only
covered Britain, Ireland and later Greece). Virtual signposts were also new
in 0.2.

Version 0.3, released on 19/9/19, contains several new features:

- Crowd-sourced augmented reality noticeboards, showing hazards (muddy paths,
blockages, aggressive cows, etc) or places of interest (views, historical 
sites) along a path.
- Routing to a given nearby POI. The calculated route is displayed in AR.
- Easier, map-based interface to select data pre-download location.
- Larger, more readable signposts.
- Different data download zoom levels. In rural areas you can now download
as much as 18km x 18km data at once; as long as data density is sparse,
signposts will still be generated quickly. On the other hand, to speed up
signpost generation in urban areas, there are two new zoom levels which
download approximately 4.5 x 4.5 km and 2.25 x 2.25 km.

Note that **Hikar is a research project** and therefore, while perfectly usable
for navigation it is not a 'polished' app with a really nice UI, and, like many
AR apps, can use a lot of battery power.

Please note that, for a currently-unknown reason, Hikar will not run on the
Amazon Fire HD 10 (and possibly other Amazon Fire devices).

Many thanks to my colleague Neil Brewis for providing the signpost assets.

To build you will also need Freemaplib (20190918 version), available at:

https://gitlab.com/nickw1/freemaplib


CREDITS
-------

Train icon for stations on signposts from the official OpenStreetMap icon set:
https://github.com/openstreetmap/map-icons

Other icons created by myself.
